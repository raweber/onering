# -*- coding: utf-8 -*-
######################################################
# Die Klasse kontrolliert für eine gegebene RNA länge 
# welche Kombinationen für gegebene Aminosäuren möglich sind.
######################################################

import copy
import AminoRad

class ProduceDNA (object):

 def __init__(self, Aminos, Length, Forbidden=[]):
  if (len(Aminos)>Length): print 'ERROR - DNAProduction - Chain too short.'; return None
  
  self.Aminos = Aminos
  self.Length = Length
  self.Forbidden = Forbidden

  self.FreeAmino = Length-len(Aminos)
  self.Proteins = ['A','C','G','U']

  # Wurde etwas gefunden? Wenn ja, werden die Listen hier abgespeichert
  self.Combinations = []
  self.Success = False

  # Starte Calculation
  self.RunCombinations(self.Length)
  self.CleanUp()


 def RunCombinations (self,length,RNA = '',DNAContent = []):
 # Rekursionsfunktion zum aufbauen eines Proteinstangs
  for Prot in self.Proteins:
   NewRNA = RNA+Prot
   NewDNAContent = copy.deepcopy(DNAContent)
   if length == 1:
    # Das Ende der Kombinationen wurde erreicht.
    self.DecodeProtString(NewRNA)
   else:
    if len(NewRNA)>2:
     NewDNAContent.append(AminoRad.GetAmino(NewRNA[-3:]))
     if not self.CheckRNA(NewDNAContent): continue
    self.RunCombinations(length-1,NewRNA,NewDNAContent)

 
 def DecodeProtString(self,RNA):
  DNAContent = AminoRad.AminoString(RNA*3)[0]
  if self.CheckRNA(DNAContent):
   # Der String ist erwünscht und kann so abgespeichert werden
   self.Success = True
   self.Combinations.append(DNAContent)


 def CheckRNA(self, DNAContent):
  # Kontrolliert ob eine unerwünschte Aminosäure enthalten ist oder ob noch genügend Freihreitsgrade zum fortsetzen übrig sind.
  Aminos = copy.deepcopy(self.Aminos)
  WrongAmis = 0

  for NOT in self.Forbidden:
   if NOT in DNAContent: return False
  
  if len(DNAContent)>self.FreeAmino:
   for Ami in DNAContent:
    if Aminos.count(Ami)>0:
     Aminos.remove(Ami)
    else:
     WrongAmis += 1
     if WrongAmis>self.FreeAmino: return False

  return True

 def CleanUp(self):
  # Der Beginn eines Rings ist nicht definiert, lösche alle Listen außer einer
  for Comb in self.Combinations:
   for i in range(len(Comb)-1):
    rot = Comb[i+1:]+Comb[:i+1]
    if rot in self.Combinations: self.Combinations.remove(rot)