# -*- coding: utf-8 -*-
######################################################
# Rückgabe des Generischen Codes nach:
# https://de.wikipedia.org/wiki/Genetischer_Code
######################################################

def GetAmino (mRNA):
 '''Erwartet einen String aus 3 Buchstaben (A,C,G,U) und gibt die entsprechende Aminosäure Abbkürzung zurück.'''
 if(not len(mRNA)==3): print('ERROR - AminoRad - Wrong mRNA given.');return None

 if(mRNA[0]=='A'):
  if(mRNA[1]=='A'):
   if(mRNA[2]=='A' or mRNA[2]=='G'): return 'K'
   elif(mRNA[2]=='C' or mRNA[2]=='U'): return 'N'
  elif (mRNA[1]=='C'): return 'T'
  elif(mRNA[1]=='G'):
   if(mRNA[2]=='A' or mRNA[2]=='G'): return 'R'
   elif(mRNA[2]=='C' or mRNA[2]=='U'): return 'S'
  elif(mRNA[1]=='U'):
   if(mRNA[2]=='G'): return 'M'
   elif(mRNA[2]=='A' or mRNA[2]=='C' or mRNA[2]=='U'): return 'I'

 elif(mRNA[0]=='C'):
  if(mRNA[1]=='A'):
   if(mRNA[2]=='A' or mRNA[2]=='G'): return 'Q'
   elif(mRNA[2]=='C' or mRNA[2]=='U'): return 'H'
  elif(mRNA[1]=='C'): return 'P'
  elif(mRNA[1]=='G'): return 'R'
  elif(mRNA[1]=='U'): return 'L'

 elif(mRNA[0]=='G'):
  if(mRNA[1]=='A'):
   if(mRNA[2]=='A' or mRNA[2]=='G'): return 'E'
   elif(mRNA[2]=='C' or mRNA[2]=='U'): return 'D'
  elif(mRNA[1]=='C'): return 'A'
  elif(mRNA[1]=='G'): return 'G'
  elif(mRNA[1]=='U'): return 'V'

 elif(mRNA[0]=='U'):
  if(mRNA[1]=='A'):
   if(mRNA[2]=='A' or mRNA[2]=='G'): return 'Stop'
   elif(mRNA[2]=='C' or mRNA[2]=='U'): return 'Y'
  elif(mRNA[1]=='C'): return 'S'
  elif(mRNA[1]=='G'):
   if(mRNA[2]=='A'): return 'Stop'
   elif(mRNA[2]=='C' or mRNA[2]=='U'): return 'C'
   elif(mRNA[2]=='G'): return 'W'
  elif(mRNA[1]=='U'):
   if(mRNA[2]=='A' or mRNA[2]=='G'): return 'L'
   elif(mRNA[2]=='C' or mRNA[2]=='U'): return 'F'

def AminoString (RNA):
 '''Erwartet einen String aus Buchstaben (A,C,G,U) und liefert eine Liste mit Aminosäuren zurück. Der zweite Rückgabewert ist der "Rest".'''

 AminoSeq = []
 for i in range(int(len(RNA)/3)):
  AminoSeq.append(GetAmino(RNA[i*3:(i+1)*3]))
 if len(RNA)%3 == 0: return (AminoSeq, '')
 else: return (AminoSeq, RNA[-(len(RNA)%3):])