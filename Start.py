# -*- coding: utf-8 -*-
######################################################
# Gewünschte Aminosäuren können angegeben werden
# es werden alle Kobinationen durchgetestet,
# sobald der minimale Ringe gefunden wurden, wird abgebrochen.
######################################################

from DNAProduction import ProduceDNA


SetAmino = ['P','G','L','K','E',]

print(str(len(SetAmino))+ ' Aminosaeuren wurden eingegeben.')
for i in range(len(SetAmino),len(SetAmino)*3):
 if i%3==0: continue
 print('Teste '+str(i)+' Proteine')
 Calculator = ProduceDNA(Aminos=SetAmino, Length=i)
 if Calculator.Success:
  print('Es konnten Ringe mit '+str(i)+' Proteinen gefunden werden:')
  for Ring in Calculator.Combinations:
   print Ring
  break